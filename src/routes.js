const express = require("express");
const router = express.Router();
const productCategoryRoutes = require("./routes/productCategories");
const productRoutes = require("./routes/products");
const userRoutes = require("./routes/users");
const authRoutes = require("./routes/auth");

router.use('/api/product-categories',productCategoryRoutes);
router.use('/api/products',productRoutes);
router.use('/api/users', userRoutes);
router.use('/api/auth', authRoutes);

module.exports = router;