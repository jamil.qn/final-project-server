module.exports = (req, res, next) => {
  if (!req.user.isAdmin)
    return res.status(403).json({ message: "دسترسی رد شد. شما ادمین نیستید" });
    next();
  };
