const cloudinary = require("../utils/cloudinary");
const PictureModel = require("../models/picture");

const uploadImagesToCloudinary = async (pictureFiles) => {
  
  const multiplePicturePromise = pictureFiles.map((picture) =>
    cloudinary.uploader.upload(picture.path)
  );
  const imageResponses = await Promise.all(multiplePicturePromise);

  return imageResponses;
};

const deleteImagesFromCloundinary = async (pictures) => {
  const deletePicturesPromise = pictures.map((picture) =>
    cloudinary.uploader.destroy(picture.cloudinary_id)
  );
  const deletePictureResponses = await Promise.all(deletePicturesPromise);

  return deletePictureResponses;
};

const createPictureRow = async (product_id, path, cloudinary_id, alt, title) => {
  const picture = new PictureModel({
    product_id,
    path,
    cloudinary_id,
    alt,
    title,
  });
  await picture.save();
  return picture;
};

const findPictureOfProduct = (id) => {
  return PictureModel.find({ product_id: id });
};

const deletePicturesRow = async (pictures) => {
  const deletePicturesPromise = pictures.map((picture) =>
  PictureModel.findByIdAndDelete(picture._id)
  );
  const deletePictureResponses = await Promise.all(deletePicturesPromise);

  return deletePictureResponses;
}

module.exports = {
  uploadImagesToCloudinary,
  createPictureRow,
  findPictureOfProduct,
  deleteImagesFromCloundinary,
  deletePicturesRow
};
