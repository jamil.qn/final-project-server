const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const userController = require("../controllers/userController");
const express = require("express");
const router = express.Router();

router.get("/me", auth, userController.getUser);

router.get("/admin", auth, admin, userController.getUser);

router.post("/", userController.createUser);

module.exports = router;
