const Joi = require("joi");
const bcrypt = require("bcrypt");
const UserModel = require("../models/user");

exports.auth = async (req, res) => {
  const { body } = req;
  const { error } = validate(body);
  if (error) return res.status(400).json({ message: error.details[0].message });

  try {
    const user = await UserModel.findOne({ email: body.email });
    if (!user) return res.status(400).json({ message: "ایمیل معتبر نمی باشد." });

    const validPassword = await bcrypt.compare(body.password, user.password);
    if (!validPassword)
      return res.status(400).json({ message: "رمز عبور معتبر نمی باشد." });

    const token = user.generateAuthToken();
    res.json({
      data: {
        token: token,
      },
    });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

const validate = (user) => {
  const schema = Joi.object({
    email: Joi.string().required().email(),
    password: Joi.string().required(),
  });

  return schema.validate(user);
};
