const {
  getAll,
  getById,
  create,
  update,
  remove,
  validateProductdata,
  makeProductJson,
} = require("../actions/products");

const {
  uploadImagesToCloudinary,
  createPictureRow,
  findPictureOfProduct,
  deleteImagesFromCloundinary,
  deletePicturesRow,
} = require("../actions/pictures");
const {getParentCategoryWithChildId} = require("../actions/productCategories");

exports.getAll = async (req, res) => {
  try {
    const products = await getAll();

    const productsWidthPictures = [];
    for (let product of products) {
      const productPictures = await findPictureOfProduct(product._id);
      const productCategory = await getParentCategoryWithChildId(product.product_category_id);
      productsWidthPictures.push(makeProductJson(product, productPictures, productCategory));
    }

    res.json({ data: productsWidthPictures });
  } catch (err) {
    res.json({ message: err.message });
  }
};

exports.getOne = async (req, res) => {
  const id = req.params.id;
  try {
    const product = await getById(id);
    if (!product)
      return res
        .status(404)
        .json({ message: "The product with this given id not found" });

    //get pictures of product
    const pictures = await findPictureOfProduct(id);
    const productCategory = await getParentCategoryWithChildId(product.product_category_id);

    res.json({ data: makeProductJson(product, pictures, productCategory) });
  } catch (err) {
    res.json({ message: err.message });
  }
};

exports.create = async (req, res) => {
  const { body, files } = req;
  const { error } = validateProductdata(body);
  if (error) return res.status(400).json({ message: error.details[0].message });

  try {
    //upload to cloudinary and save in picture collection
    const uploadResult = await uploadImagesToCloudinary(files);
    const product = await create(body);
    if (!product)
      return res.status(400).json({
        message:
          "Not found product category same record of product_categories collection",
      });

    const pictures = [];
    for (let picture of uploadResult) {
      pictures.push(
        await createPictureRow(
          product._id,
          picture.secure_url,
          picture.public_id,
          product.name,
          product.name
        )
      );
    }

    const productCategory = await getParentCategoryWithChildId(product.product_category_id);

    res.json({ data: makeProductJson(product, pictures, productCategory) });
  } catch (err) {
    res.json({ message: err.message });
  }
};

exports.update = async (req, res) => {
  const { body, params, files } = req;
  const id = params.id;

  const { error } = validateProductdata(body);
  if (error) return res.status(400).json({ message: error.details[0].message });

  try {
    const product = await update(id, body);
    if (!product)
      return res
        .status(404)
        .json({ message: "The product with this given id not found" });

    // //find pictures of product and delete from cloudinary
    const picturesOfProduct = await findPictureOfProduct(id);
    // const deleteResultFromCloudinary = await deleteImagesFromCloundinary(
    //   picturesOfProduct
    // );
    // //delete from picutres collection
    // const deleteResult = await deletePicturesRow(picturesOfProduct);

    // const pictures = [];
    // if (deleteResultFromCloudinary && deleteResult) {
    //   //upload to cloudinary and save in picture collection
    //   const uploadResult = await uploadImagesToCloudinary(files);

    //   for (let picture of uploadResult) {
    //     pictures.push(
    //       await createPictureRow(
    //         product._id,
    //         picture.secure_url,
    //         picture.public_id,
    //         product.name,
    //         product.name
    //       )
    //     );
    //   }
    // }

    const productCategory = await getParentCategoryWithChildId(product.product_category_id);

    res.json({ data: makeProductJson(product, picturesOfProduct, productCategory) });
  } catch (err) {
    res.json({ message: err.message });
  }
};

exports.delete = async (req, res) => {
  const id = req.params.id;
  try {
    const product = await remove(id);

    if (!product)
      return res
        .status(404)
        .json({ message: "The product with this given id not found" });

    //find pictures of product and delete from cloudinary
    const picturesOfProduct = await findPictureOfProduct(id);
    const deleteResultFromCloudinary = await deleteImagesFromCloundinary(
      picturesOfProduct
    );
    //delete from picutres collection
    const deleteResult = await deletePicturesRow(picturesOfProduct);
    
    const productCategory = await getParentCategoryWithChildId(product.product_category_id);

    if (deleteResultFromCloudinary && deleteResult) {
      res.json({
        message: "item deleted",
        data: makeProductJson(product, pictures, productCategory),
      });
    }
  } catch (err) {
    res.json({ message: err.message });
  }
};
