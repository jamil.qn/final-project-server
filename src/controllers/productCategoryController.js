const {
  getAllForNavbarMegaMenu,
  getAll,
  getById,
  create,
  update,
  remove,
  validateProductCategorydata,
} = require("../actions/productCategories");

exports.getAllForNavbarMegaMenu = async (req, res) => {
  req.params;
  try {
    const productCategories = await getAllForNavbarMegaMenu();
    res.json({ data: productCategories });
  } catch (err) {
    res.json({ message: err.message });
  }
};

exports.getAll = async (req, res) => {
  try {
    const productCategories = await getAll();
    res.json({ data: productCategories });
  } catch (err) {
    res.json({ message: err.message });
  }
};

exports.getOne = async (req, res) => {
  const id = req.params.id;
  try {
    const category = await getById(id);
    res.json({ data: category });
  } catch (err) {
    res.json({ message: err.message });
  }
};

exports.create = async (req, res) => {
  const { body } = req;
  const { error } = validateProductCategorydata(body);
  if (error) return res.status(400).json({ message: error.details[0].message });

  try {
    const productCategory = await create(body);
    if (!productCategory)
      return res.json({
        message:
          "Not found parent same record of product_categories collection",
      });

    res.json({ data: productCategory });
  } catch (err) {
    res.json({ message: err.message });
  }
};

exports.update = async (req, res) => {
  const { body, params } = req;
  const id = params.id;

  const { error } = validateProductCategorydata(body);
  if (error) return res.status(400).json({ message: error.details[0].message });

  try {
    const productCategory = await update(id, body);
    if (!productCategory)
      return res
        .status(404)
        .json({ message: "The product category with this given id not found" });

    res.json({ data: productCategory });
  } catch (err) {
    res.json({ message: err.message });
  }
};

exports.delete = async (req, res) => {
  const id = req.params.id;
  try {
    const productCategory = await remove(id);

    if (!productCategory)
      return res
        .status(404)
        .json({ message: "The product category with this given id not found" });

    res.json({ message: "دسته با موفقیت پاک شد", data: productCategory });
  } catch (err) {
    res.json({ message: err.message });
  }
};
