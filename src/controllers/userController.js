const bcrypt = require("bcrypt");
const { getUser, validateUser } = require("../actions/users");
const UserModel = require("../models/user");

exports.getUser = async (req, res) => {
  try {
    const user = await getUser(req.user._id);
    res.json({ data: user });
  } catch (err) {
    res.json({ messageError: err.message });
  }
};

exports.createUser = async (req, res) => {
  const { body } = req;
  const { error } = validateUser(body);
  if (error) return res.status(400).json({ message: error.details[0].message });

  try {
    let user = await UserModel.findOne({ email: body.email });
    if (user)
      return res.status(400).json({ message: "User already registered." });

    user = new UserModel(body);

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);

    await user.save();

    const token = user.generateAuthToken();

    res.header("x-auth-token", token).json({ data: user });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};
