const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);

const ProductCategoryModel = require("../models/productCategory");
const ProductModel = require("../models/product");
const {
  findPictureOfProduct,
  deleteImagesFromCloundinary,
  deletePicturesRow,
} = require("../actions/pictures");

const getAllForNavbarMegaMenu = async () => {
  const categories = [];
  const mainCategories = await ProductCategoryModel.find({ parent_id: null });
  for (let category of mainCategories) {
    categories.push({
      maincategory: category,
      subCategories: await ProductCategoryModel.find({
        parent_id: category._id,
      }),
    });
  }
  return categories;
};

const getAll = async () => {
  return await ProductCategoryModel.find();
};

const getById = async (id) => {
  return await ProductCategoryModel.findById(id);
};

const getParentCategoryWithChildId = async (id) => {
  const subCategory = await ProductCategoryModel.findById(id);
  const mainCategory = await ProductCategoryModel.findOne({
    _id: subCategory.parent_id,
  });
  if (subCategory && mainCategory) {
    return {
      mainCategory,
      subCategory,
    };
  }
};

const create = async (data) => {
  if (data.parent_id) {
    const res = await ProductCategoryModel.findById(data.parent_id);
    if (!res) return false;
  }

  const productCategory = new ProductCategoryModel(data);
  await productCategory.save();
  return productCategory;
};

const update = async (id, { name, parent_id }) => {
  const productCategory = await ProductCategoryModel.findByIdAndUpdate(
    id,
    {
      $set: {
        name,
        parent_id,
      },
    },
    { new: true }
  );

  if (!productCategory) return false;

  return productCategory;
};

const remove = async (id) => {
  const productCategory = await ProductCategoryModel.findByIdAndDelete(id);

  if (!productCategory) return false;

  if (!productCategory.parent_id) {
    const subCategories = await ProductCategoryModel.find({ parent_id: id });
    for (let subCategory of subCategories) {
      await ProductCategoryModel.findByIdAndDelete(subCategory._id);
      await removeAllProductsForCategory(subCategory._id);
    }
  } else {
    await removeAllProductsForCategory(id);
  }

  return productCategory;
};

const removeAllProductsForCategory = async (id) => {
  const products = await ProductModel.find({ product_category_id: id });

  for (let product of products) {
    await ProductModel.findByIdAndDelete(product._id);
    //find pictures of product and delete from cloudinary
    const picturesOfProduct = await findPictureOfProduct(product._id);
    await deleteImagesFromCloundinary(picturesOfProduct);
    //delete from picutres collection
    await deletePicturesRow(picturesOfProduct);
  }
};

const validateProductCategorydata = (data) => {
  const haveParentId = () =>
    data.parent_id ? Joi.objectId().required() : null;

  const schema = Joi.object({
    name: Joi.string().max(45).required(),
    parent_id: haveParentId(),
  });
  return schema.validate(data);
};

module.exports = {
  getAllForNavbarMegaMenu,
  getAll,
  getById,
  create,
  update,
  remove,
  getParentCategoryWithChildId,
  validateProductCategorydata,
};
