const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);
const ProductModel = require("../models/product");
const ProductCategoryModel = require("../models/productCategory");

const getAll = async () => {
  return await ProductModel.find().sort('-_id');
};

const getById = async (id) => {
  return await ProductModel.findById(id);
};

const create = async (data) => {
  const res = await ProductCategoryModel.findById(data.product_category_id);
  if (!res) return false;

  const product = new ProductModel(data);
  await product.save();
  return product;
};

const update = async (
  id,
  {
    product_category_id,
    name,
    price,
    discount,
    inventory,
    description,
    specification,
  }
) => {
  const product = await ProductModel.findByIdAndUpdate(
    id,
    {
      $set: {
        product_category_id,
        name,
        price,
        discount,
        inventory,
        description,
        specification,
      },
    },
    { new: true }
  );

  if (!product) return false;

  return product;
};

const remove = async (id) => {
  const product = await ProductModel.findByIdAndDelete(id);

  if (!product) return false;

  return product;
};

const validateProductdata = (data) => {
  const schema = Joi.object({
    product_category_id: Joi.objectId().required(),
    name: Joi.string().max(191).required(),
    price: Joi.number().required(),
    discount: Joi.number().min(0).max(100).required(),
    inventory: Joi.number().required(),
    description: Joi.string().required(),
    specification: Joi.string().required(),
  });
  return schema.validate(data);
};

const makeProductJson = (
  {
    _id,
    name,
    price,
    discount,
    inventory,
    description,
    specification,
  },
  pictures,
  product_category
) => {
  return {
    _id,
    product_category,
    name,
    price,
    discount,
    inventory,
    description,
    specification,
    pictures,
  };
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove,
  validateProductdata,
  makeProductJson,
};
