const mongoose = require("mongoose");

const productCategorySchema = new mongoose.Schema({
  name: {
    type: String,
    maxlength: 45,
    required: true,
  },
  parent_id: {
    type: mongoose.Schema.Types.ObjectId,
  },
});

const ProductCategory = mongoose.model(
  "Product_category",
  productCategorySchema
);

module.exports = ProductCategory;
