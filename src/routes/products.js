const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const express = require("express");
const router = express.Router();
const upload = require("../utils/multer");
const productController = require("../controllers/productController");

router.get("/", productController.getAll);

router.get("/:id", productController.getOne);

router.post(
  "/",
  auth,
  admin,
  upload.array("pictures", 10),
  productController.create
);

router.put(
  "/:id",
  auth,
  admin,
  upload.array("pictures", 10),
  productController.update
);

router.delete("/:id", auth, admin, productController.delete);

module.exports = router;
