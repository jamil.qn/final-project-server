module.exports = (req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://185.208.172.195');
  // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Accept, Authorization');
  next();
}