const bodyParser = require("body-parser");
// const cors = require("./config/cors");
const cors = require('cors');
const express = require("express");
const app = express();
const dotenv = require('dotenv');
dotenv.config();

const routes = require("./routes");
const connectdb = require("./utils/db");

connectdb();

// app.use(cors);
app.use(cors());
app.use(
  bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 50000,
  })
);
app.use(bodyParser.json({ limit: "50mb" }));
app.use(routes);

const port = process.env.PORT || 8080;
console.log("Magic happens on port " + port);
app.listen(port, () => {
  console.log(`This application listening on port ${port}...`);
});
