const mongoose = require("mongoose");

const connectdb = async () => {
  mongoose
    // .connect("mongodb://localhost:27017/xantiayadak-db", {
    .connect(process.env.DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(() => console.log("Connected to MongoDB..."))
    .catch((err) => console.error("Could not connect to MongoDB...", err));
};

module.exports = connectdb;
