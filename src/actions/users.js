const Joi = require("joi");
const UserModel = require("../models/user");

const getUser = async (id) => {
  return await UserModel.findOne({_id: id}).select("-password");
}

const validateUser = (user) => {
  const schema = Joi.object({
    name: Joi.string().min(5).max(50),
    email: Joi.string().min(5).max(255).email(),
    password: Joi.string().min(5).max(255),
    isAdmin: Joi.boolean().default(false)
  });

  return schema.validate(user);
}

module.exports = {
  getUser,
  validateUser,
}