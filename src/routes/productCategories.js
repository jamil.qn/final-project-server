const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const express = require("express");
const router = express.Router();
const productcategoryController = require("../controllers/productCategoryController");

router.get("/navbar-menu", productcategoryController.getAllForNavbarMegaMenu);
router.get("/", productcategoryController.getAll);

router.get("/:id", productcategoryController.getOne);

router.post("/", auth, admin, productcategoryController.create);

router.put("/:id", auth, admin, productcategoryController.update);

router.delete("/:id", auth, admin, productcategoryController.delete);

module.exports = router;
